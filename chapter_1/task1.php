<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<?php
const TIMEZONE_KIEV = 'Europe/Kiev';

if (date_default_timezone_get() != TIMEZONE_KIEV){
    date_default_timezone_set(TIMEZONE_KIEV);
    init();
}else init();

function init(){
    if (date("H") < 3 || date("H") >= 11){
        echo "Добрий ранок";
    }else echo "Добрий вечір";
}
?>
</body>
</html>