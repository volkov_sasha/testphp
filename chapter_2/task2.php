<!DOCTYPE html>
<html>
<head>
    <title>Task2</title>
    <meta charset="UTF-8">
</head>
<body>
<form action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
    Введіть Email: <input type="text" name="email"><br>
    <input type="submit">
</form>
<?php
if (isset($_POST['email'])) {
    $email = trim($_POST['email']);

    if (strpos($email, "@")>=1) {
        $sub1 = substr($email, strpos($email, "@")+1, strlen($email));
        if (strpos($sub1, ".")>= 1 && substr($sub1, strlen($sub1)-1) != ".") {
            echo '<span style="color:#00FF00;">Введена коректна електронна адреса</span>';
        } else {
            echo '<span style="color:#FF0000;">Електронна адреса повинна містити після @ хочаб одну '
                . 'крапку але не останій символ</span>';
        }
    } else {
        echo '<span style="color:#FF0000;">Некоректра електронна адреса, невистачає @ </span>';
    }

} else {
    exit();
}
?>
</body>
</html>
